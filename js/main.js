function validateEmail(email) {

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(email);

}


function LegalTz(num) {
        var tot = 0;
        var tz = new String(num);
        for (i=0; i<8; i++)
            {
                x = (((i%2)+1)*tz.charAt(i));
                if (x > 9) 
                    {
                    x =x.toString();
                    x=parseInt(x.charAt(0))+parseInt(x.charAt(1))
                    }
            tot += x;
            }
        
    if ((tot+parseInt(tz.charAt(8)))%10 == 0) {
        console.log("תקין");
        return true;
    } else {
         
        console.log("לא תקין")
        return false;
    }
}


function validatePhone(elementValue){  

        var phoneNumberPattern = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s.]{0,1}[0-9]{3}[-\s.]{0,1}[0-9]{4}$/;  

        if(phoneNumberPattern.test(elementValue) == false)

        {

             var phoneNumberPattern = /^(\()?\d{3}(\))?(.|\s)?\d{3}(.|\s)\d{4}$/; 

             return phoneNumberPattern.test(elementValue);   

        }

        return phoneNumberPattern.test(elementValue);  

    }

    function getParameterRef(name, url) {
        if (!url) {
          url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return '';
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
function getQueryVariable()

{

       var query = window.location.search.substring(1);

       var vars = query.split("&");

       for (var i=0;i<vars.length;i++) {

               var pair = vars[i].split("=");

			   $("#form").append($("<input/>").attr('type','hidden').attr('name',pair[0] ).val(pair[1]));

       } 

}

$(document).ready(function(){

getQueryVariable();

	$(document).on("submit","#form",function(event){




		var $e 				= $(this),

			$fullname 		= $e.find("#fullname"),

			$email 			= $e.find("#email"),

			$phone 			= $e.find("#phone"),

			feedback 		= '<h4 class="feedback">תודה על פנייתך! <br> נציג יצור איתך קשר בהקדם.</h4>';



			$fullname.removeClass("error");

			$email.removeClass("error");

			$phone.removeClass("error");

			if( $fullname.val().length == 0 ){



				$fullname.addClass("error");



			}

			if( $phone.val().length == 0 || !validatePhone( $phone.val() ) ){



				$phone.addClass("error");



			}


			if( $email.val().length == 0 || !validateEmail( $email.val() ) ){



				$email.addClass("error");



			}



			if( 

				$fullname.val() == 0 ||

				$email.val() == 0 ||

				$phone.val() == 0 || 

				!validatePhone( $phone.val() ) || 

				!validateEmail( $email.val() )

			){

				return false;

			}



			$("#form").hide();




			$("._form").append(feedback);



			 //   $.ajax({

				// 	type        : 'POST',

				// 	url         : $("#form").attr('action'), 

				// 	data        : $("#form").serializeObject(),  

				// 	encode: true

				// })

				// 	.done(function(data) { 


				// })


			event.preventDefault();
			// return true;






	});


	// $.fn.serializeObject = function()
	// {
	// 	var o = {};
	// 	var a = this.serializeArray();
	// 	$.each(a, function() {
	// 		if (o[this.name] !== undefined) {
	// 			if (!o[this.name].push) {
	// 				o[this.name] = [o[this.name]];
	// 			}
	// 			o[this.name].push(this.value || '');
	// 		} else {
	// 			o[this.name] = this.value || '';
	// 		}
	// 	});
	// 	return o;
	// };


	$("#fullname,#email,#phone").on("input",function(){



		var $elem  = $(this).val().length;



		if( $elem == 0 ){



			$(this).addClass("error");



		}

		else{



			$(this).removeClass("error");



		}



	});




});
